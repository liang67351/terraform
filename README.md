# GovTech Assessment 

Challenge 1.
Creating an infrastructure on AWS with following requirements 
- Three instances running on application load balancer with auto scaling group
- A VPC with public and private multi-az subnets 
- RDS MySQL with multi-az


# Architecture Diagram
![diagram](https://i.ibb.co/sm92TGS/Screenshot-from-2021-07-13-16-10-51.png)
