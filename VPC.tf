resource "aws_vpc" "Demo_VPC" {
    cidr_block = "172.16.0.0/16"
    instance_tenancy = "default"
    enable_dns_support = "true"
    enable_dns_hostnames = "true"
    enable_classiclink = "false"

    tags = {
        Name = "Demo-VPC"
    }
}

locals {
    public_subnet = {
        "172.16.1.0/24" = "ap-southeast-1a"
        "172.16.2.0/24" = "ap-southeast-1b"
    }

    private_subnet = {
        "172.16.3.0/24" = "ap-southeast-1a"
        "172.16.4.0/24" = "ap-southeast-1b"
    }
}

resource "aws_subnet" "Demo_public_subnet" {
    for_each = local.public_subnet
    vpc_id = aws_vpc.Demo_VPC.id
    cidr_block = each.key
    availability_zone = each.value
    map_public_ip_on_launch = "true"

    tags = {
        Name = "Demo-public-subnet"
    }
}

resource "aws_subnet" "Demo_private_subnet" {
    for_each = local.private_subnet
    vpc_id = aws_vpc.Demo_VPC.id
    cidr_block = each.key
    availability_zone = each.value
    map_public_ip_on_launch = "false"

    tags = {
        Name = "Demo-private-subnet"
    }
}

resource "aws_internet_gateway" "Demo_igw"{
    vpc_id = aws_vpc.Demo_VPC.id

    tags = {
        Name = "Demo-igw"
    }

}

resource "aws_route_table" "Demo_public_routetable" {
    vpc_id = aws_vpc.Demo_VPC.id

    tags = {
        Name = "Demo-public-routetable"
    }
}

resource "aws_route_table" "Demo_private_routetable" {
    vpc_id = aws_vpc.Demo_VPC.id

    tags = {
        Name = "Demo-private-routetable"
    }
}

resource "aws_route_table_association" "Demo_rta_public"{
    for_each = aws_subnet.Demo_public_subnet
    subnet_id = each.value.id
    route_table_id = aws_route_table.Demo_public_routetable.id
} 

resource "aws_route_table_association" "Demo_rta_private"{
    for_each = aws_subnet.Demo_private_subnet
    subnet_id = each.value.id
    route_table_id = aws_route_table.Demo_private_routetable.id
}

resource "aws_route" "Demo_route_igw"{
    route_table_id = aws_route_table.Demo_public_routetable.id
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.Demo_igw.id
}


resource "aws_db_subnet_group" "Demo_subnetgroup" {
    name       = "demo_subnetgroup"
    subnet_ids = [for d in aws_subnet.Demo_private_subnet: d.id]

    tags = {
        Name = "My DB subnet group"
    }
}
