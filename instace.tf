resource "aws_launch_configuration" "Demo_asg_launchConfig" {
    name    =   "ASG_launchConfig"
    image_id    =   "ami-018c1c51c7a13e363"
    instance_type = "t2.micro"
    key_name = <Key pair name>
    security_groups = [aws_security_group.Demo_web_SG.id]
    user_data = <<EOF
		#!/bin/bash
	yum update -y
	yum install httpd curl git rsync -y
	service httpd start
	chkconfig httpd on
	sudo git init
    EOF
    lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "Demo_asg" {
    name    =   "ASG"
    launch_configuration    =   aws_launch_configuration.Demo_asg_launchConfig.id
    max_size    =   3
    min_size    =   3
    vpc_zone_identifier =   [for d in aws_subnet.Demo_public_subnet: d.id]

    lifecycle {
        ignore_changes = [load_balancers, target_group_arns]
    }
}

resource "aws_lb" "Demo_ALB" {
    name = "Demo-application-load-balancer"
    load_balancer_type = "application"
    subnets =  [for d in aws_subnet.Demo_public_subnet: d.id]
    security_groups = [aws_security_group.Demo_web_SG.id]
}

resource "aws_lb_listener" "Demo_lblistener" {
    load_balancer_arn = aws_lb.Demo_ALB.arn
    protocol = "HTTP"
    port = "80"

    default_action {
        type = "forward"
        target_group_arn = aws_lb_target_group.Demo_lb_target.arn
    }
}

resource "aws_lb_target_group" "Demo_lb_target" {
    name = "Demo-lb-targetgroup"
    port = "80"
    protocol = "HTTP"
    vpc_id = aws_vpc.Demo_VPC.id
}

resource "aws_autoscaling_attachment" "Demo_asg_attch" {
    autoscaling_group_name  =   aws_autoscaling_group.Demo_asg.id
    alb_target_group_arn    =   aws_lb_target_group.Demo_lb_target.arn
}
/*
resource "aws_lb_target_group_attachment" "Demo_lb_attach" {
    for_each = aws_instance.web-instance
    port = "80"
    target_id = each.value.id
    target_group_arn = aws_lb_target_group.Demo_lb_target.arn
}
*/
