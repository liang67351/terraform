provider "aws" {
  profile    = "default"
  region     = "ap-southeast-1"
  access_key = <IAM User access key>
  secret_key = <IAM User secret key>
}
