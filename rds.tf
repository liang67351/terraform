resource "aws_db_instance" "Demo_mysql" {
    identifier  =   "mysqldb"
    storage_type    =   "gp2"
    allocated_storage   =   20

    engine  =   "mysql"
    engine_version  =   "8.0"
    instance_class  =   "db.t3.micro"

    name    =   "mysqldb"
    username    =   "admin"
    password    =   "password"

    db_subnet_group_name    =   aws_db_subnet_group.Demo_subnetgroup.id
    vpc_security_group_ids  =   [aws_security_group.Demo_db_SG.id]
    multi_az    =   true

    skip_final_snapshot  = true

    tags = {
        Name    =   "Demo-mysql-rds-instance"
    }

}