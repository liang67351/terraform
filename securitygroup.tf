resource "aws_security_group" "Demo_VPC_securitygroup"{
    vpc_id      = aws_vpc.Demo_VPC.id
    name        = "VPC_Security_Group"
    description = "VPC_Security_Group"
    depends_on  = [aws_vpc.Demo_VPC]

    ingress {
        from_port = "0"
        to_port   = "0"
        protocol  = "-1"
        self      = true
    }

    egress {
        from_port = "0"
        to_port   = "0"
        protocol  = "-1"
        self      = "true"
    }

    tags = {
        Name = "Demo-vpc-SecurityGroup"
    }
}

locals {
  Demo_SecurityGroup = {
    "22" = ["0.0.0.0/0"]
    "80" = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "Demo_web_SG"{
    vpc_id = aws_vpc.Demo_VPC.id
    name= "Web_Security_Group"
    description = "Web_Security_Group"

    dynamic "ingress" {
        for_each = local.Demo_SecurityGroup
        content {
            from_port = ingress.key
            to_port = ingress.key
            cidr_blocks = ingress.value
            protocol = "tcp"
        }
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "Demo-web-SecurityGroup"
    }
}

resource "aws_security_group" "Demo_db_SG"{
    vpc_id = aws_vpc.Demo_VPC.id
    name= "DB_Security_Group"
    description = "DB_Security_Group"

    ingress {
        from_port = "3306"
        to_port = "3306"
        protocol = "tcp"

        security_groups = [aws_security_group.Demo_web_SG.id]
    }

    tags = {
        Name = "Demo-db-SecurityGroup"
    }
}